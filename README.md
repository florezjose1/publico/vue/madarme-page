# Madarme Page

Version node: 11.15.0

## Project setup
```
npm install
```

## Configure settings firebase

* Create project in firebase
* Copy settings project by web
* In file `src/plugins/firebase.js` paste settings
 


### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```


---
Power with ❤  by [Jose Flórez](https://joseflorez.co)