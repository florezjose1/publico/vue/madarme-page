import Vue from 'vue'
import Vuex from 'vuex'
import {db, st} from '../plugins/firebase'
import {eventBus} from './../utils/EventBus'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        base_page: {
            title: 'Editar título',
            courses: [{title: 'título curso'}],
            social_network: [
                {
                    'title': 'Nombre red social',
                    'url': 'https://www.facebook.com',
                    'icon': 'fa-facebook-f',
                },
            ],
            imageURL: require('./../assets/fondo.jpg'),
            copyright: '© 2020 - Jose Flórez (www.joseflorez.co)'
        },
        page: {},

    },
    mutations: {
        SET_DATA_STATE: (state, payload) => {
            state.page = Object.assign({}, state.page, payload)
        },
    },
    actions: {
        getPage(context) {
            context.state.page = [];
            return new Promise((resolve, reject) => {
                db.ref('madarme').once('value').then(snapshot => {
                    let data = snapshot.val();
                    if (data) {
                        for (let key in data) {
                            data[key]['ref'] = key;
                            context.commit('SET_DATA_STATE', data[key])
                        }
                    } else {
                        context.commit('SET_DATA_STATE', context.state.base_page)
                    }

                    resolve();
                }).catch(error => reject(error))
            });
        },
        addPage(context, {image}) {
            return new Promise((resolve, reject) => {
                let key = context.state.page.ref;
                db.ref(`madarme/${key}`).update(context.state.page)
                    .then(() => {
                        if (image) {
                            const fileName = image.name;
                            const ext = fileName.slice(fileName.lastIndexOf('.'));
                            const upload = st.ref('madarme/' + key + '.' + ext).put(image)

                            upload.on('state_changed', function (snapshot) {
                                    let progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                                    eventBus.$emit('IndicatorProgress', progress);
                                }, error => {
                                    console.log(error)
                                },
                                () => {
                                    upload.snapshot.ref.getDownloadURL().then((imageURL) => {
                                        db.ref('madarme').child(key).update({imageURL: imageURL})
                                            .then(() => {
                                                context.commit('SET_DATA_STATE', {
                                                    ...context.state.page,
                                                    imageUrl: imageURL,
                                                    key: key
                                                });
                                                eventBus.$emit('HideIndicatorProgress');
                                                resolve();
                                            })
                                    })
                                })

                        } else {
                            resolve();
                        }

                    }).catch((e) => reject(e));
            });
        }

    },
    modules: {}
})
